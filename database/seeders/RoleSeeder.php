<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
//        $role_admin = Role::create(['name' => 'admin', 'guard_name' => 'api']);
//        $role_standard = Role::create(['name' => 'user', 'guard_name' => 'api']);
//
//        $permission_read = Permission::create(['name' => 'list-user', 'guard_name' => 'api']);
//        $permission_write = Permission::create(['name' => 'create-user', 'guard_name' => 'api']);
//        $permission_edit = Permission::create(['name' => 'edit-user', 'guard_name' => 'api']);
//        $permission_delete = Permission::create(['name' => 'delete-user', 'guard_name' => 'api']);
//
//        $permission_admin = [$permission_read, $permission_write, $permission_edit, $permission_delete];
//
//        $role_admin->syncPermissions($permission_admin);
//        $role_standard->givePermissionTo($permission_read);

        $permissions = Permission::defaultPermissions();
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission, "guard_name" => "api"]);
        }
        $this->command->info('Default permissions added.');

        $roles = Role::allRoles();

        foreach ($roles as $role){
            $role = Role::create(['name' => $role, 'guard_name' => 'api']);
            switch ($role->name){
                case Role::ADMIN:
                    $role->syncPermissions(Permission::all());
                    break;
                case Role::USER:
                    $role->syncPermissions(Permission::where(function ($query){
                        $query->where('name','LIKE', 'view_user')
                            ->orWhere('name', 'LIKE', 'view_role')
                            ->orWhere('name', 'LIKE', 'view_table')
                            ->orWhere('name', 'LIKE', 'view_order');
                    })->get());
            }
            $this->command->info('Adding user with teams ...');
        }
        $this->command->warn('All done: ');
    }
}
