<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = [
            [
                'name' => 'Admin',
                'username' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => '12345678',
                'phone' => '0987654321',
                'role' => 'admin'
            ], [
                'name' => 'User',
                'username' => 'user',
                'email' => 'user@gmail.com',
                'password' => '12345678',
                'phone' => '0123456789',
                'role' => 'user'
            ]
        ];

        foreach ($users as $user) {
            $create_user = User::create([
                'name' => $user['name'],
                'username' => $user['username'],
                'email' => $user['email'],
                'password' => Hash::make($user['password']),
                'phone' => $user['phone']
            ]);

            $create_user->assignRole($user['role']);
        }
    }
}
