<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Table;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TableController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $table = Table::orderBy('id', 'DESC');
        if ($request->get('search')){
            $table->where('name', 'LIKE', '%'.$request->get('search').'%');
        }

        if ($request->get('perPage')){
            $perPage = $request->get('perPage');
        }else{
            $perPage = 5;
        }
        if ($request->get('page')){
            $getTable = $table->paginate($perPage);
        }else{
            $getTable = $table->get();
        }
        return response()->json(['table' => $getTable]);
    }

    public function create(Request $request): JsonResponse
    {
        $input = $request->all();
        $validator = Validator::make($input, [
           'name' => 'required|max:100'
        ]);
        if ($validator->fails())
            return response()->json($validator->errors());
        $table = Table::create([
            'name' => $request->name
        ]);
        return response()->json(['table' => $table]);
    }

    public function show($id): JsonResponse
    {
        if (!Table::where('id', $id)->exists()){
            return response()->json(['Error'=>'Table id: '.$id.' not found!']);
        }
        $table = Table::find($id);
        return response()->json(['table' => $table]);
    }

    public function update(Request $request, $id): JsonResponse
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required|max:100'
        ]);
        if ($validator->fails())
            return response()->json($validator->errors());

        if (!Table::where('id', $id)->exists()){
            return response()->json(['Error'=>'Table id: '.$id.' not found!']);
        }
        $table = Table::find($id);
        $table->fill($input)->save();

        return response()->json(['table' => $table]);
    }

    public function delete($id): JsonResponse
    {
        if (!Table::where('id', $id)->exists()){
            return response()->json(['Error'=>'Table id: '.$id.' not found!']);
        }
        $table = Table::find($id);
        $table->delete();
        return response()->json(['table id: '.$id.' is deleted!']);
    }

}
