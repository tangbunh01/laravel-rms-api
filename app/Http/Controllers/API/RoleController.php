<?php

namespace App\Http\Controllers\API;

use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use App\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $roles = Role::whereNotIn('name', ['admin'])->with('permissions');
        if ($request->get('search')) {
            $roles->where('name', 'LIKE', '%' . $request->get('search') . '%');
        }
        if ($request->get('perPage')) {
            $perPage = $request->get('perPage');
        } else {
            $perPage = 5;
        }
        if ($request->get('page')) {
            $getRoles = $roles->orderBy('id', 'DESC')->paginate($perPage);
        } else {
            $getRoles = $roles->get();
        }

        return response()->json(['roles' => $getRoles]);
    }

    public function create(Request $request): JsonResponse
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required|max:255|unique:roles',
            'permission' => 'exists:permissions,id'
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()]);
        }

        $role = Role::create([
            'name' => $request->name,
            'guard_name' => 'api'
        ]);


        if ($request->get('permission')) {
            $permissions = $request->get('permission');
        } else
            $permissions = [1, 5, 9, 13];

        $role->syncPermissions($permissions);


        return response()->json(['role' => $role]);
    }

    public function show($id): JsonResponse
    {

        if (!Role::where('id', $id)->exists()) {
            return response()->json(['error' => " role id = " . $id . ' not found'], 400);
        }
        $role = Role::with('permissions')->find($id);

        return response()->json(['role' => $role], 200);
    }

    public function update(Request $request, $id): JsonResponse
    {
        if (!Role::where('id', $id)->exists()) {
            return response()->json(['error' => " role id = " . $id . ' not found'], 400);
        }

        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => ['required', 'max:100', Rule::unique(Role::class)->ignore($id)],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $role = Role::findById($id);
        $role->fill($input)->save();

        return response()->json(['role' => $role]);
    }

    public function delete($id)
    {
        if (!Role::where('id', $id)->exists()) {
            return response()->json(['error' => " role id = " . $id . ' not found'], 400);
        }

        $role = Role::findById($id);
        $role->delete();
        return response()->json(['role' => 'role id: ' . $id . ' is deleted!']);
    }

    public function update_permissions(Request $request, $id)
    {
        $input = $request->all();
        $validators = Validator::make($input, [
            "permissions" => 'exists:permissions,id'
        ]);

        if ($validators->fails()){
            return response()->json($validators->errors());
        }

        $role = Role::with('permissions')->find($id);
        $addPermission = [];
        $removePermission = [];

        $permissions = $request->get('permissions');
        foreach ($permissions as $permission) {
            if ($permission['status']) {
                $addPermission[] = $permission['id'];
            } else
                $removePermission[] = $permission['id'];
        }
        $role->syncPermissions($addPermission);
        $role->revokePermissionTo($removePermission);

        return response()->json(['role' => $role]);
    }
}
