<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules;

class AuthenticationController extends Controller
{
    public function register(Request $request): JsonResponse
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required|max:255|string',
            'username' => 'required|max:255|string|unique:' . User::class,
            'phone' => 'nullable|string|max:50',
            'email' => 'required|max:255|lowercase|email|unique:' . User::class,
            'password' => ['required', 'max:255', 'min:8', 'confirmed', Rules\Password::defaults()]
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()]);
        }

        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone
        ]);
        if ($request->role)
            $user->assignRole($request->role);
        else
            $user->assignRole('user');

        $token = $user->createToken('Token')->accessToken;

        return response()->json(['Token' => $token]);

    }

    public function login(Request $request): JsonResponse
    {
        $input = $request->only('email', 'password');
        $validator = Validator::make($input, [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors()]);
        }

        if (Auth::attempt($input)) {
            $user = Auth::user();
            $token = $user->createToken('Token', ['edit'])->accessToken;
            return response()->json(['user' => $user, 'Token' => $token], 200);
        } else {
            return response()->json(['error' => 'Invalid email or password'], 401);
        }

    }

    public function profile(): JsonResponse
    {
        $user = auth()->user();
//        $permissions = $user->getAllPermissions();
        return response()->json(['user' => $user], 200);
    }

    public function logout(): JsonResponse
    {
        auth()->user()->tokens()->delete();
        return response()->json(['message' => 'Successfully logged out'], 200);
    }
}
