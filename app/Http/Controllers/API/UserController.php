<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules;

class UserController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $users = User::with('roles')->withoutRole('admin');
        $users->orderBy('id', 'DESC');

        if ($request->get('search'))
            $users->where('name', 'LIKE', '%' . $request->get('search') . '%')
                ->orWhere('username', 'LIKE', '%' . $request->get('search') . '%')->get();

        if ($request->get('role')) {
            $users->whereHas('roles', function ($query) use ($request) {
                $query->where('name', $request->get('role'));
            });
        }
        if ($request->get('perPage')) {
            $perPage = $request->get('perPage');
        } else {
            $perPage = 5;
        }
        if ($request->get('page')) {
            $getUsers = $users->paginate($perPage);
        } else {
            $getUsers = $users->get();
        }

        return response()->json(['users' => $getUsers], 200);
    }

    public function store(Request $request): JsonResponse
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required|max:255|string',
            'username' => 'required|max:255|string|unique:' . User::class,
            'phone' => 'nullable|string|max:50',
            'email' => 'required|max:255|lowercase|email|unique:' . User::class,
            'password' => ['required', 'max:255', 'min:8', 'confirmed', Rules\Password::defaults()],
            'role' => 'exists:roles,name'
        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()]);
        }
//        if ($request->get('role'))
//            if (!Role::where('name', $request->get('role'))->exists()) {
//                return response()->json(['Error' => 'Role not found!'], 400);
//            }
        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone
        ]);
        if ($request->role) {
            $user->assignRole($request->role);
        } else
            $user->assignRole('user');

        return response()->json(['user' => $user]);
    }

    public function show($id): JsonResponse
    {
        if (!User::where('id', $id)->exists()) {
            return response()->json(['error' => 'user id: ' . $id . ' not found'], 400);
        }
        $user = User::with('roles')->find($id);
        return response()->json(['user' => $user]);
    }

    public function update(Request $request, $id): JsonResponse
    {
        if (!User::where('id', $id)->exists()) {
            return response()->json(['error' => 'user id: ' . $id . ' not found'], 400);
        }
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required|max:255|string',
            'username' => ['required', 'max:255', 'string', Rule::unique(User::class)->ignore($id)],
            'phone' => 'nullable|string|max:50',
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', Rule::unique(User::class)->ignore($id)],

        ]);
        if ($validator->fails()) {
            return response()->json([$validator->errors()]);
        }
        $user = User::findOrFail($id);
        $user->fill($input)->save();

        if (!$user->hasRole($request->role)) {
            $user->syncRoles($request->role);
        }
        return response()->json(['user' => $user]);
    }

    public function delete($id): JsonResponse
    {
        if (!User::where('id', $id)->exists()) {
            return response()->json(['error' => 'user id: ' . $id . ' not found'], 400);
        }
        $user = User::find($id);
        $user->delete();
        return response()->json(['message' => 'user id = ' . $id . ' is deleted']);
    }


}
