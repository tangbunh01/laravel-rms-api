<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthenticationController;
use App\Http\Controllers\API\RoleController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\TableController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/auth/register', [AuthenticationController::class, 'register']);
Route::post('/auth/login', [AuthenticationController::class, 'login']);

Route::middleware('auth:api')->group(function (){

    Route::get('/auth/profile', [AuthenticationController::class, 'profile']);
    Route::post('/auth/logout', [AuthenticationController::class, 'logout']);

    // Route User
    Route::get('/user', [UserController::class, 'index']);
    Route::post('/user/store', [UserController::class, 'store']);
    Route::get('/user/{id}', [UserController::class, 'show']);
    Route::put('/user/{id}', [UserController::class, 'update']);
    Route::delete('/user/{id}', [UserController::class, 'delete']);

    // Route Role
    Route::get('/role', [RoleController::class, 'index']);
    Route::post('/role', [RoleController::class, 'create']);
    Route::get('/role/{id}', [RoleController::class, 'show']);
    Route::put('/role/{id}', [RoleController::class, 'update']);
    Route::delete('role/{id}', [RoleController::class, 'delete']);
    Route::put('role/permission/{id}', [RoleController::class, 'update_permissions']);

    //Route Table
    Route::get('/table', [TableController::class, 'index']);
    Route::post('/table', [TableController::class, 'create']);
    Route::get('/table/{id}', [TableController::class, 'show']);
    Route::put('/table/{id}', [TableController::class, 'update']);
    Route::delete('/table/{id}', [TableController::class, 'delete']);
});
